<?php

namespace Th20\SmsGateway;

use DirectoryIterator;
use RegexIterator;
use RuntimeException;


class SmsSpooler
{

    private $sender;

    private $spoolDir;


    public function __construct($dir, SmsSender $sender)
    {
        if (!is_dir($dir) || !is_writable($dir)) {
            throw new RuntimeException('SMS spool directory is not writable.');
        }

        $this->sender = $sender;
        $this->spoolDir = $dir;
    }

    public function spoolSms($to, $text)
    {
        $file = tempnam($this->spoolDir, 'sms_');
        file_put_contents($file, serialize(array(
            'to' => $to,
            'text' => $text,
        )));
    }

    public function sendAllSpooled()
    {
        $count = 0;

        $iterator = new DirectoryIterator($this->spoolDir);
        $iterator = new RegexIterator($iterator, '/^sms_/');

        foreach ($iterator as $file) {
            $sms = unserialize(file_get_contents($file->getPathName()));

            if ($this->sendSpooledSms($sms)) {
                $count++;
            }

            unlink($file->getPathName());
        }
        return $count;
    }

    protected function sendSpooledSms(array $sms)
    {
        $to = $sms['to'];
        $text = $sms['text'];

        return $this->sender->sendSms($to, $text);
    }

}
