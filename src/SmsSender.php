<?php

namespace Th20\SmsGateway;

use InvalidArgumentException;

use Buzz\Browser;
use Buzz\Client\Curl as CurlClient;
use Symfony\Component\HttpKernel\Log\DebugLoggerInterface;


class SmsSender
{

    const MESSAGE_TYPE = 'SMS:TEXT';


    private $debug;

    private $logger;

    private $username;

    private $password;

    private $url;


    public function __construct($username, $password, $url)
    {
        $this->debug = false;
        $this->password = $password;
        $this->url = $url;
        $this->username = $username;
    }

    public function setDebugMode($mode)
    {
        $this->debug = (bool) $mode;
    }

    public function setLogger(DebugLoggerInterface $logger = null)
    {
        $this->logger = $logger;
    }

    public function sendSms($to, $text)
    {
        try {
            $to = $this->sanitizeRecipient($to);
        } catch (InvalidArgumentException $e) {
            return false;
        }

        if (empty($text)) {
            return false;
        }

        if ($this->logger) {
            $log = sprintf('Sending SMS to %s: %s', $to, $text);
            $this->logger->info($log);
        }

        if (!$this->debug) {
            return $this->envoySms($this->username, $this->password, $to, $text);
        } else {
            return true;
        }
    }

    protected function sanitizeRecipient($phone)
    {
        $phone = preg_replace('/[^0-9]/', '', trim(strval($phone)));
        if (!preg_match('/^[0-9]{11}$/', $phone)) {
            throw new InvalidArgumentException('Invalid phone number.');
        }
        return $phone;
    }

    protected function envoySms($username, $password, $recipient, $text, $type = self::MESSAGE_TYPE)
    {
        $originator = $this->generateOriginator($recipient);

        if (empty($username) || empty($password) || empty($recipient) || empty($text)) {
            return false;
        }

        $fields = array(
            'action' => 'sendmessage',
            'username' => $username,
            'password' => $password,
            'recipient' => $recipient,
            'messagetype' => $type,
            'originator' => $originator,
            'messagedata' => $text,
        );

        $client = new CurlClient();
        $client->setVerifyPeer(false);
        $client->setOption(CURLOPT_SSL_VERIFYHOST, false);

        $browser = new Browser($client);
        $browser->submit($this->url, $fields);

        return true;
    }

    protected function generateOriginator($phone)
    {
        return 'INFO_KAZ';
    }

}
